import com.fz.mapper.StudentMapper;
import com.fz.util.MybatisUtil;
import org.apache.ibatis.session.SqlSession;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * Created by Administrator on 2017/7/5.
 */
public class Demo {
    protected SqlSession ss;
    protected StudentMapper stu;
    @Before
    public void bef(){
         this.ss =  new MybatisUtil().getSs();
         stu = this.ss.getMapper(StudentMapper.class);
    }
    @After
    public void close(){
        this.ss.commit();
        this.ss.close();
    }

    @Test
    public void t1(){
//        Student s1 = stu.queryById(2);
//        System.out.println(s1);
//        System.out.println(s1.getName());
//        System.out.println(s1.getAge());

//        List<Student> list =  stu.queryAll();
//        System.out.println(list.size());
//        for(Student m:list){
//            System.out.println(m.getName());
//            System.out.println(m.getAge());
//            System.out.println("***********************");
//        }

//        List<Student> list = stu.query("科比",27);
//        System.out.println(list.size());

//        int i = stu.deleteById(8);
//        System.out.println(i);

//        Student s = new Student();
//        s.setAge(11);
//        s.setName("姚明");
//        int i = stu.add(s);

//        Map<String ,Object> s = new HashMap<String, Object>();
//        s.put("name","易建联");
//        s.put("age",30);
//        stu.add1(s);

//        Student s = new Student();
//        s.setName("易建联");
//        s.setAge(42);
//        s.setId(11);
//        stu.update(s);

//        Student s = stu.queryById(10);
//        s.setName("kai");
//        stu.update(s);

//        Map<String,Object> s = new HashMap<String ,Object>();
//        s.put("name","kai1");
//        s.put("id",10);
//        stu.updateById(s);

        //物理分页
//        Map<String,Integer> p = new HashMap<String, Integer>();
//        int currpage = 3;
//        int pagesize = 2;
//        p.put("start",currpage*pagesize-pagesize);
//        p.put("pagesize",pagesize);
//        List<Student> ss = stu.page(p);
//        for(Student s:ss){
//            System.out.println(s);
//        }

        //逻辑分页
//        RowBounds n = new RowBounds(2,2);
//        List<Student> bks = stu.page1(n);
//        for(Student s: bks){
//            System.out.println(s.getName());
//        }

//        stu.delstudent(19);

//        Map<String ,Object> m = new HashMap<String, Object>();
//        m.put("name","汤普森");
//        m.put("age",27);
//        stu.addstudent(m);

//        List<Student> st = stu.showstudent();
//        System.out.println(st.size());
//        for(Student b : st){
//            System.out.println(b.getName());
//        }

//        Map<String,Object> m = new HashMap<String,Object>();
//        stu.studentcount(m);
//        System.out.println(m.get("r"));

//        Map<String,Object> mm= new HashMap<String, Object>();
//        mm.put("name","%科%");
//        List<Student> m=stu.querya(mm);
//        System.out.println(m);

//        Map<String,Object> mm= new HashMap<String, Object>();
//        mm.put("name","%科%");
//        List<Student> m = stu.query1(mm);
//        System.out.println(m);

//        Map<String,Object> mm= new HashMap<String, Object>();
//        mm.put("name","%科%");
//        List<Student> m = stu.query1(mm);
//        System.out.println(m);

        
    }
}
