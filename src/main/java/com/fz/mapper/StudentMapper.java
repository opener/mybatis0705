package com.fz.mapper;

import com.fz.entity.Student;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 2017/7/5.
 */

public interface StudentMapper {
    //普通方法
//  @Select("select * from student where id = #{id}")
    public Student queryById(int id);
    public List<Student> queryAll();
    public List<Student> query(@Param("name") String name,@Param("age") int age);
//  @Delete("delete from student where id = #{id}")
    public int deleteById(int id);
    public int add(Student student);
    public int add1(Map<String ,Object> student);
    public int update(Student student);
    public int updateById(Map<String,Object> student);
    public List<Student> page(Map<String,Integer> map);
    public List<Student> page1(RowBounds rowBounds);

    //存储过程
    public int delstudent(int did);
    public int addstudent(Map<String,Object> map);
    public List<Student> showstudent();
    public void studentcount(Map<String,Object> map);

    //动态语句
    public List<Student> querya();
    public List<Student> querya(Map<String,Object> map);

    public List<Student> query1();
    public List<Student> query1(Map<String,Object> map);

    public int update1(Map<String,Object> map);

    public List<Student> query2();
    public List<Student> query2(Map<String,Object> map);

    public int delete1();
    public int delete1(Map<String,Object> map);
}
