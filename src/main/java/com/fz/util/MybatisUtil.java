package com.fz.util;

import lombok.Data;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by Administrator on 2017/7/5.
 */
@Data
public class MybatisUtil {
    private SqlSessionFactory sf;
    private SqlSession ss;

    public MybatisUtil(){
        String resource = "mybatis-config.xml";
        try {
            InputStream inputStream = Resources.getResourceAsStream(resource);
            this.sf= new SqlSessionFactoryBuilder().build(inputStream);
            this.ss=this.sf.openSession();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
