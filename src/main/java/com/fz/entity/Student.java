package com.fz.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by Administrator on 2017/7/5.
 */
@Data @AllArgsConstructor @NoArgsConstructor
public class Student {
    private int id;
    private String name;
    private int age;
}
